package com.mindtree.maven;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


public class MavenBuildTest
{
	ProgressBar pg = new ProgressBar();
	WebDriver wb = new ChromeDriver();
	@BeforeTest
	private void start() {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\vasu\\OneDrive\\Desktop\\AutomationCCon12-07\\Maven\\Driver\\chromedriver.exe");
		wb.get("https://www.google.com");	
		System.out.println("vasu");
	}
	@Test
	private void test() 
	{
		WebElement element =wb.findElement(By.xpath("//input[@class='gLFyf gsfi']"));
		element.sendKeys("Amazon login");
		element.sendKeys(Keys.ENTER);
		wb.findElement(By.xpath("//div[@class='cfxYMc JfZTW c4Djg MUxGbd v0nnCb']")).click();
		
	}
	@AfterMethod
	private void progress()
	{
		pg.fill();
	}
	@AfterTest
	private void after() {
		wb.close();
	}

}
